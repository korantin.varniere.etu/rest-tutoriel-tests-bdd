package fr.ulille.iut.todo.ressource;

import java.sql.SQLException;

import fr.ulille.iut.todo.BDDFactory;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

/**
 * BDDClearRessource
 * Cette ressource permet de détruire toutes les tables de la base pour
 * repartir à zéro. A enlever pour la mise en production (quand le schéma
 * de base de données est stable).
 */
@Path("clearDatabase")
public class BDDClearRessource {

    @GET
    public void clearDatabase()  throws SQLException {
        BDDFactory.dropTables();
    }
}
